# config_checker

config_checker can be used to search for multiple paramters in muliple different sources. 
These parameters should be of an standard pythontype like for example int, dict or str.
Json-files, dotenv-files, env-variables and the ServiceRegistry.


## Naming convention
For env-variables one should use snake-case for python variable names, as the variables from the environment are [transformed to lower case and dashes are replaced by underscores](https://confz.readthedocs.io/en/latest/reference/sources.html#confz.EnvSource) and they need to match the python variable names.

## Usage

First all the needed modules have to be imported.
```python
from confz import BaseConfig, FileSource, EnvSource
from config_checker.sr_source import SR_Source
```

Now the paramters and their supposed type  are defined in the Basic_Config class, so that they can searched through the sources. Basic sources can be added directly in CONFIG_SOURCES. Nevertheless they always need an extra variable to be actual sources, for example the path to the dotenv-file(example.env).
```python
class Basic_Config(BaseConfig):
    dot_env_file: str = "default_env-file"
    env_variable: str = "default_env-var"
    etcd: str = "default_env_etcd"
    default: str = "default"

    CONFIG_SOURCES = [SR_Source({"etcd": "key_path"}), EnvSource(allow_all=True, file="example.env")]
```
The result of this search can now be accessed like:
```python
print("Basic Output of the Config: ", Basic_Config())
```
In addition to that it is also possible to append additional Sources or search parameter. 
An extra search parameter (extra_parameter) is now added to the basic search parameters of the Basic_Config.
```python
class Extended_Config(Basic_Config):
    extra_parameter: str = "default"
```

Extra sources to search in can be added, like below:
```python
print("Extended Config: ", Extended_Config(config_sources=[FileSource(file="example.json")]))
```
## SR_Source

This custom made source uses a dict to search through the ServiceRegistry for certain paramters. The structure of the dict can be easliy shown through the following example:
```python
search_dict = {"parameter you want to search for":"the key, which should lead to the correspondig value in etcd "}
```


