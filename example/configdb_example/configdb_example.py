from config_checker import ConfigDBSource, Json, BaseConfig


# The ConfigDB can also be used as a Source for the Config_Checker like below
class DB_Config(BaseConfig):
    optoboard_config: Json = "nothing found"
    configdb_object_id: str = "nothing found"
    configdb_payload_id: str = "nothing found"

    # The ConfigDB_Source firstly needs a list of dict in which the identifier,type and search_dict have to defined. Secoundly the configdb_key is needed.
    CONFIG_SOURCES = [
        ConfigDBSource(
            configdb_key="demi/default/itk-demo-configdb/api/url",
            runkey="my_new_runkey",
            search_list=[
                {
                    "object_type": "optoboard",
                    "config_type": "opto_config",
                    "search_dict": {"serial": 300},
                    "var": "optoboard_config",
                }
            ],
        )
    ]


#  Accessing the wanted Variable
print(DB_Config())
