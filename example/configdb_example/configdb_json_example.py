import config_checker as cc


# The ConfigDB can also be used as a Source for the Config_Checker like below
class FelixSettings(cc.BaseConfig):
    felix_app: int = -1
    felix_card_number: int = -1
    felix_device_number: int = -1
    felix_initialize: int = -1
    dryrun: int = -1
    noflx: int = -1
    felix_config_file: str = "nothing found"
    felix_data_interface: str = "nothing found"
    felix_toflx_ip: cc.IPType = "nothing found"
    felix_tohost_ip: cc.IPType = "nothing found"
    felix_tohost_dcs_pages: int = -1
    serial: int = -1
    configdb_object_id: str = "nothing found"
    configdb_payload_id: str = "nothing found"


# The ConfigDB_Source firstly needs a dict in which the identifier, type and search_dict have to defined.
# Secondly the configdb_key and runkey are needed.
config = FelixSettings(
    config_sources=[
        cc.ConfigDBJsonSource(
            configdb_key="demi/default/itk-demo-configdb/api/url",
            runkey="my_new_runkey",
            search={"object_type": "felixDevice", "config_type": "felix_meta", "search_dict": {"serial": 124}},
        )
    ]
)
print(config)
