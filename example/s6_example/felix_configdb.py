import config_checker as cc
import json
from example.s6_example.settings_classes import FelixSettings, FelixConfig, ConfigSupport, FILE_DIR

FILE_DIR = "."


class UseConfigDB(cc.BaseConfig):
    use_configdb: bool = True

    CONFIG_SOURCES = cc.EnvSource(allow_all=True)


def main():
    if UseConfigDB().use_configdb:
        config_support = ConfigSupport()
        felix_config = FelixConfig(
            config_sources=[
                cc.ConfigDBJsonSource(
                    config_support.configdb_key,
                    config_support.runkey,
                    {"search_dict": json.loads(config_support.search_dict), "object_type": "felix", "config_type": "config"},
                ),
            ]
        )

        with open(f"{FILE_DIR}/felix_config.json", "w") as f:
            f.write(json.dumps(felix_config.model_dump())) # for pydantic 1 use json.dumps(felix_config.dict())

        felix_settings = FelixSettings(
            config_sources=[
                cc.ConfigDBJsonSource(
                    config_support.configdb_key,
                    config_support.runkey,
                    {"search_dict": json.loads(config_support.search_dict), "object_type": "felix", "config_type": "meta"},
                )
            ]
        )

    else:
        felix_settings = FelixSettings(config_sources=cc.EnvSource(allow_all=True))
        felix_config = FelixConfig(config_sources=cc.FileSource(file=f"{FILE_DIR}/{felix_settings.felix_config_file}"))

    settings_dict = felix_settings.model_dump() # for pydantic 1 use felix_settings.dict()

    for key, value in settings_dict.items():
        print(key.upper())
        print(value)

    with open(f"{FILE_DIR}/felix_settings.json", "w") as f:
        f.write(json.dumps(settings_dict))


if __name__ == "__main__":
    main()
