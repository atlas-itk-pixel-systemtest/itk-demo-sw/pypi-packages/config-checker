import config_checker as cc

FILE_DIR = "/config"  # os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))


class ConfigSupport(cc.BaseConfig):
    runkey: str = "my_runkey"
    search_dict: str = '{"serial":123}'
    configdb_key: str = "demi/default/itk-demo-configdb/api/url"

    CONFIG_SOURCES = cc.EnvSource(allow_all=True)


class FelixSettings(cc.BaseConfig):
    felix_app: int = 0
    felix_card_number: int = 0
    felix_device_number: int = 0
    felix_initialize: int = 0
    dryrun: int = 0
    noflx: int = 0
    felix_config_file: str = "felix_config.json"
    felix_data_interface: str = "lo"
    felix_toflx_ip: cc.IPType = "localhost"
    felix_tohost_ip: cc.IPType = "localhost"
    felix_tohost_dcs_pages: int = 0
    serial: int = 0


class Links(cc.BaseConfig):
    polarity: int
    icec: int
    rx: list[int]
    tx: list[int]


class FelixConfig(cc.BaseConfig):
    FelixID: int
    DeviceID: int
    Links: dict[str, Links]
