import config_checker as cc
from felix_configdb import FelixSettings, ConfigSupport

if __name__ == "__main__":
    path = ConfigSupport().file_dir + "/felix_settings.json"

    felix = FelixSettings(config_sources=[cc.FileSource(file=path), cc.EnvSource(allow_all=True)])

    settings_dict = {
        key: value
        for key, value in felix.__dict__.items()
        if not key.startswith("__") and not callable(value) and not callable(getattr(value, "__get__", None))
    }
    for key, value in settings_dict.items():
        print(key)
        print(value)

    # for key, value in felix.felix_settings_file.model_dump().items():
    #     print(key)
    #     print(value)
