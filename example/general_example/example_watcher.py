from config_checker import BaseConfig
from config_checker import SR_watch_Source
from service_registry.registry import ServiceRegistry
import time

sr = ServiceRegistry()


class example_watcher(BaseConfig):
    target: str = "default"

    CONFIG_SOURCES = SR_watch_Source({"target": "target"})


sr.put("target", "Miss")
print(sr.get("target"))
print(example_watcher())
