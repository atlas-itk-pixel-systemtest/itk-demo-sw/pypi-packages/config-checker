from config_checker import BaseConfig, FileSource, EnvSource
from config_checker import SR_Source
import os
from service_registry.registry import ServiceRegistry


# Putting the values in the dotenv_file and the enviroment
os.environ["env_variable"] = "env_value"
sr = ServiceRegistry()
sr.put("key_path", "etcd_value")


# Creating a basic Config with some variables to search for
class Basic_Config(BaseConfig):
    # standard search parameter => parameter_to_search_for : exspected_parameter_type = default_value
    dot_env_file: str = "default_env-file"
    env_variable: str = "default_env-var"
    json_file: str = "default_jsonfile"
    default: str = "default"

    # Adding basic sources to search in
    CONFIG_SOURCES = [FileSource(file="example/general_example/example.json"), EnvSource(allow_all=True, file="example/general_example/example.env")]


print("\n\nBasic Output of the Config:\n", Basic_Config())


# An extra search parameter (env_json) is now added to the basic search parameters of the Basic_Config
class Extended_Config(Basic_Config):
    SR: str = "default_SR"


# Extra sources to search in can also be added, like below
print("\n\nExtended Config:\n", Extended_Config(config_sources=[SR_Source({"SR": "key_path"})]))

# The different variables can be accessed by using
print("\n\nSingle variable of a config:\n", Basic_Config().dot_env_file)
