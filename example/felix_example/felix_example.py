from confz import BaseConfig, FileSource


class Links(BaseConfig):
    polarity: int
    icec: int
    rx: list[int]
    tx: list[int]


class FelixConfig(BaseConfig):
    FelixID: str
    DeviceID: int
    Links: dict[str, Links]


conf = FelixConfig(config_sources=[FileSource(file="example/flx.json")])
print(conf)
