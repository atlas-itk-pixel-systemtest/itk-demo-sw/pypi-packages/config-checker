# Changelog

All notable changes[^1] to the `config-checker` package.

This repo is "living at head" which means tags correspond to the overall repo.
All subrepos/workspaces, all packages in the package registry and all container images in the image registry follow the repo tag.

[^1]: The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

(always keep this section to take note of untagged changes at HEAD)

## [2.0.3] - 2024-11-18
- Bugfix for python 3.9 compatibility

## [2.0.2] - 2024-11-18
- Updated dependencies
- Read sr_url from env vars when using configdb as source

## [2.0.1] - 2024-08-26
- Updated dependencies

## [2.0.0] - 2024-08-21
- Updated pyconfigdb dependency to work with new version of configdb
- Updated dependency format
- Disabled SR watch source (for now)

## [1.1.3] - 2024-07-10
- Updated pyconfigdb dependency

## [1.1.2] - 2024-04-24
- Added id of configdb object and payload to Objects populated using the ConfigDBSources (as configdb_object_id and configdb_payload_id)

## [1.1.1] - 2024-04-23
- Uses pyconfigdb to access configdb-api

## [1.1.0] - 2024-04-23
- Adaptations for configdb tag 1.7.0

## [1.0.2] - 2024-03-13
- Updated dependencies

## [1.0.1] - 2023-12-14
- Improved configdb error messages

## [1.0.0] - 2023-12-12
- Update to newest service-registry version
  - removes flask dependencies etc.

## [0.4.4] - 2023-11-30
- Added new version of configDB source that allows to check the content of a json file directly against a model
  - Without needing to wrap it in a base class (like for the standard configDB source)