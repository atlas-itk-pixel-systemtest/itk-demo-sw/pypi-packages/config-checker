from dataclasses import dataclass
from confz import ConfigSource
from confz.loaders import Loader, register_loader
from pyregistry import ServiceRegistry
from pyregistry.exceptions import SRException


@dataclass
class SR_watch_Source(ConfigSource):
    keys_dict: dict


# defining the data that the custom source provides
class SR_watch_Loader(Loader):
    @classmethod
    def populate_config(cls, config: dict, confz_source: SR_watch_Source):
        sr = ServiceRegistry()
        config_update = {}
        for keys, searchkey in confz_source.keys_dict.items():

            def callback(
                event_type,
                key,
                value,
            ):
                config_update[keys] = value
                print(value)
                cls.update_dict_recursively(config, config_update)

            try:
                found_value = sr.watch(searchkey, callback)
            except SRException:
                pass


# registration of the new Source
register_loader(SR_watch_Source, SR_watch_Loader)
