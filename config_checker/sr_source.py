from dataclasses import dataclass
from confz import ConfigSource
from confz.loaders import Loader, register_loader
from pyregistry import ServiceRegistry
from pyregistry.exceptions import SRException


# defining a custom source that searches in etcd; {example_parameter : etcd_key} => {example_parameter : etcd_value}
@dataclass
class SR_Source(ConfigSource):
    """
    Additional source for the config_checker, which gets its values from etcd and stores them.

    Parameters
    ----------
    keys_dict : dict
        Looks like this {"variable": "key to find the value for the variable in etcd"}

    """

    keys_dict: dict


# defining the data that the custom source provides
class SR_Loader(Loader):
    @classmethod
    def populate_config(cls, config: dict, confz_source: SR_Source):
        sr = ServiceRegistry()
        config_update = {}
        for keys, searchkey in confz_source.keys_dict.items():
            try:
                found_value = sr.get(searchkey)
                config_update[keys] = found_value
            except SRException:
                pass
        cls.update_dict_recursively(config, config_update)


# registration of the new Source
register_loader(SR_Source, SR_Loader)
