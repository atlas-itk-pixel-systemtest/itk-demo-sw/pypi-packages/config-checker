from dataclasses import dataclass
from confz import ConfigSource, BaseConfig
from confz.loaders import Loader, register_loader
from pyconfigdb.configdb import ConfigDB
from pyconfigdb.exceptions import ConfigDBResponseError
from json import JSONDecodeError, loads
import logging, os


# Some logging for better Output
class CustomFormatter(logging.Formatter):
    grey = "\x1b[38;20m"
    yellow = "\x1b[33;20m"
    red = "\x1b[31;20m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"
    format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s (%(filename)s:%(lineno)d)"

    FORMATS = {
        logging.DEBUG: grey + format + reset,
        logging.INFO: grey + format + reset,
        logging.WARNING: yellow + format + reset,
        logging.ERROR: red + format + reset,
        logging.CRITICAL: bold_red + format + reset,
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(CustomFormatter())
logger.addHandler(ch)


# Dictionary consisting of the needed infos
class DictDef(BaseConfig):
    search_dict: dict
    object_type: str
    config_type: str


@dataclass
class ConfigDBJsonSource(ConfigSource):
    """
    Additional source for the config_checker, which gets its values from the ConfigDB.

    Parameters
    ----------
    configdb_key: str
        The key used to access the ConfigDB.
     
    srUrl: str
        URL of the service registry.

    runkey: str
        Name of the runkey or ID of the root node

    search: DictDef
        List of dictionaries, one for each config to get from the database.
        search_dict: dict
            Dict containing the search parameters and values (e.g. {"serial": "1"})
        object_type: str
            Type of the object to search for (e.g. "optoboard")
        config_type: str
            Type of the config to search for (e.g. "config")
    """

    configdb_key: str
    sr_url: str
    runkey: str
    search: DictDef


# defining the data that the custom source provides
class ConfigDBJsonLoader(Loader):
    @classmethod
    def populate_config(cls, config: dict, confz_source: ConfigDBJsonSource):

        config_update = {}
        db = ConfigDB(confz_source.configdb_key, confz_source.sr_url)

        dct = confz_source.search

        try:
            objects = db.search(
                identifier=confz_source.runkey,
                search_dict=dct["search_dict"],
                object_type=dct["object_type"],
                config_type=dct["config_type"],
            )
        except ConfigDBResponseError:
            logger.error(f"Runkey {confz_source.runkey} not found.")
            return

        search_string = f"search_dict: {dct['search_dict']} "
        if "object_type" in dct:
            search_string = f"{search_string}, object_type: {dct['object_type']}"
        if "config_type" in dct:
            search_string = f"{search_string}, config_type: {dct['config_type']}"

        if len(objects) == 0:
            logger.error(f"No config found for search_dict: {search_string}.")
            return
        else:
            if len(objects) != 1:
                logger.warning(f"Multiple objects found for search_dict: {search_string}, first one will be used.")

            payloads = objects[0]["payloads"]
            config_update["configdb_object_id"] = objects[0]["id"]

            if len(payloads) == 0:
                logger.error(f"No config found for search_dict: {search_string}.")
                return
            else:
                if len(payloads) != 1:
                    logger.warning(
                        f"Multiple payloads found for search_dict: {search_string}, first one will be used."
                    )

                payload = payloads[0]["data"]
                config_update["configdb_payload_id"] = objects[0]["id"]

        try:
            found_value = loads(payload)
        except JSONDecodeError:
            logger.error(
                "Config is not valid JSON. If you want to load a config with a different format, please use the ConfigDBSource."
            )
            return

        
        for key, value in found_value.items():
            config_update[key] = value

        cls.update_dict_recursively(config, config_update)


# registration of the new Source
register_loader(ConfigDBJsonSource, ConfigDBJsonLoader)
