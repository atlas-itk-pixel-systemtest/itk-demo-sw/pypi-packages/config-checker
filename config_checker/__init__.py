from confz import BaseConfig, EnvSource, FileSource
from pydantic import Json, DirectoryPath
from pydantic.networks import IPvAnyAddress
from .sr_source import SR_Source
# from .sr_watch_source import SR_watch_Source
from .ConfigDBJsonSource import ConfigDBJsonSource
from .ConfigDBSource import ConfigDBSource
from typing import Union, Literal, ClassVar

IPType = Union[IPvAnyAddress, Literal["localhost"]]
